(function () {
  
  let loader = document.getElementById("loader");
  let rotation = 0;
  console.log(loader);

  function rotateLoader() {
    rotation = rotation + 6.5;
    loader.style.transform = "rotate(" + rotation + "deg)";
  }

  setInterval(rotateLoader, 10);
})();
